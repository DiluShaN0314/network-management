-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2022 at 06:04 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `finger`
--

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

CREATE TABLE `access` (
  `auto` int(11) NOT NULL,
  `level` tinyint(4) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `id` bigint(20) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `be` tinyint(4) DEFAULT NULL,
  `uuid` varchar(36) DEFAULT NULL,
  `user` bigint(20) DEFAULT NULL,
  `creator` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `auto` int(11) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `be` tinyint(4) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `postal` int(11) DEFAULT NULL,
  `town` int(11) DEFAULT NULL,
  `street_1` varchar(100) DEFAULT NULL,
  `street_2` int(11) NOT NULL,
  `no` varchar(20) DEFAULT NULL,
  `apartment` varchar(6) DEFAULT NULL,
  `floor` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `agree`
--

CREATE TABLE `agree` (
  `auto` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `user` bigint(20) DEFAULT NULL,
  `id` bigint(20) DEFAULT NULL,
  `be` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `api`
--

CREATE TABLE `api` (
  `auto` int(11) NOT NULL,
  `code` varchar(64) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created` timestamp NULL DEFAULT current_timestamp(),
  `user` bigint(20) DEFAULT NULL,
  `device` int(11) DEFAULT NULL,
  `browser` int(11) DEFAULT NULL,
  `private` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `browser`
--

CREATE TABLE `browser` (
  `auto` int(11) NOT NULL,
  `brower` text DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `auto` int(11) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `name` int(11) DEFAULT NULL,
  `be` tinyint(4) DEFAULT NULL,
  `ids` int(11) DEFAULT NULL,
  `user` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `categories_be`
--

CREATE TABLE `categories_be` (
  `auto` int(11) NOT NULL,
  `text` varchar(60) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created` timestamp NULL DEFAULT current_timestamp(),
  `id` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `categories_name`
--

CREATE TABLE `categories_name` (
  `auto` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `text` varchar(60) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `lang` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `auto` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `id` bigint(20) DEFAULT NULL,
  `be` tinyint(4) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chat_message`
--

CREATE TABLE `chat_message` (
  `auto` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `chat` int(11) DEFAULT NULL,
  `sender` int(11) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `reply` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chat_seen`
--

CREATE TABLE `chat_seen` (
  `auto` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `id` bigint(20) DEFAULT NULL,
  `be` tinyint(4) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `message` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chat_to`
--

CREATE TABLE `chat_to` (
  `auto` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `id` bigint(20) DEFAULT NULL,
  `be` tinyint(4) DEFAULT NULL,
  `chat` int(11) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `auto` int(11) NOT NULL,
  `text` text DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created` timestamp NULL DEFAULT current_timestamp(),
  `id` bigint(20) DEFAULT NULL,
  `be` int(11) DEFAULT NULL,
  `title` int(11) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `user` bigint(20) DEFAULT NULL,
  `reply` int(11) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL,
  `uuid` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `auto` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `auto` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `type` tinyint(4) DEFAULT NULL,
  `value` varchar(50) DEFAULT NULL,
  `id` bigint(20) DEFAULT NULL,
  `be` tinyint(4) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `description`
--

CREATE TABLE `description` (
  `auto` int(11) NOT NULL,
  `id` bigint(20) DEFAULT NULL,
  `be` tinyint(4) DEFAULT NULL,
  `text` text DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `user` bigint(20) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL,
  `uuid` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `auto` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `file` int(11) DEFAULT NULL,
  `id` bigint(20) DEFAULT NULL,
  `be` tinyint(4) DEFAULT NULL,
  `uuid` varchar(36) DEFAULT NULL,
  `user` bigint(20) DEFAULT NULL,
  `caption` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `duration`
--

CREATE TABLE `duration` (
  `auto` int(11) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `be` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `duration_for`
--

CREATE TABLE `duration_for` (
  `auto` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `duration` int(11) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `per` tinyint(4) DEFAULT NULL,
  `user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `auto` int(11) NOT NULL,
  `mime` varchar(12) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `format` varchar(6) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `dir` varchar(128) DEFAULT NULL,
  `uploder` bigint(20) DEFAULT NULL,
  `uniq` varchar(32) DEFAULT NULL,
  `actual` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `hash`
--

CREATE TABLE `hash` (
  `auto` int(11) NOT NULL,
  `code` varchar(64) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created` timestamp NULL DEFAULT current_timestamp(),
  `id` int(11) DEFAULT NULL,
  `be` tinyint(4) DEFAULT NULL,
  `uuid` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `icons`
--

CREATE TABLE `icons` (
  `auto` int(11) NOT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `user` int(11) DEFAULT NULL,
  `id` bigint(20) DEFAULT NULL,
  `be` tinyint(4) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `color` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `auto` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `request` int(11) DEFAULT NULL,
  `data` text DEFAULT NULL,
  `user` bigint(20) DEFAULT NULL,
  `response` text DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `logs_online`
--

CREATE TABLE `logs_online` (
  `auto` int(11) NOT NULL,
  `time` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `ip` varchar(46) DEFAULT NULL,
  `user` bigint(20) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `logs_time`
--

CREATE TABLE `logs_time` (
  `auto` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT 1,
  `user` bigint(20) DEFAULT NULL,
  `id` int(11) DEFAULT NULL,
  `be` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL,
  `modifier` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `auto` int(11) NOT NULL,
  `body` longtext DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `sender` bigint(20) DEFAULT NULL,
  `sent` datetime DEFAULT NULL,
  `title` int(11) DEFAULT NULL,
  `type` tinyint(4) DEFAULT 1,
  `reply` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `message_receiver`
--

CREATE TABLE `message_receiver` (
  `auto` int(11) NOT NULL,
  `message` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `user` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `auto` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `id` int(11) DEFAULT NULL,
  `be` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `user` bigint(20) DEFAULT NULL,
  `schedule` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `notification_room`
--

CREATE TABLE `notification_room` (
  `auto` bigint(11) NOT NULL,
  `id` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `be` tinyint(4) DEFAULT NULL,
  `created` timestamp NULL DEFAULT current_timestamp(),
  `uuid` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE `participants` (
  `auto` int(11) NOT NULL,
  `user` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created` int(11) DEFAULT NULL,
  `id` int(11) DEFAULT NULL,
  `be` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `priority`
--

CREATE TABLE `priority` (
  `auto` int(11) NOT NULL,
  `id` bigint(20) DEFAULT NULL,
  `be` int(11) DEFAULT NULL,
  `level` tinyint(4) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` tinyint(4) DEFAULT 1,
  `user` bigint(20) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `reminder`
--

CREATE TABLE `reminder` (
  `auto` int(11) NOT NULL,
  `id` bigint(20) DEFAULT NULL,
  `be` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime NOT NULL DEFAULT current_timestamp(),
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `reminder_to`
--

CREATE TABLE `reminder_to` (
  `auto` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `remind_to` bigint(20) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `snooze` int(11) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `slug`
--

CREATE TABLE `slug` (
  `auto` int(11) NOT NULL,
  `text` varchar(128) DEFAULT NULL,
  `id` bigint(20) DEFAULT NULL,
  `be` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `auto` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `auto` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `user` bigint(20) DEFAULT NULL,
  `fname` varchar(50) DEFAULT NULL,
  `lname` varchar(50) DEFAULT NULL,
  `mname` varchar(50) DEFAULT NULL,
  `sname` varchar(50) DEFAULT NULL,
  `parent` bigint(20) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `oname` varchar(50) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `nationality` int(11) DEFAULT NULL,
  `marital` tinyint(4) DEFAULT NULL,
  `uuid` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_display`
--

CREATE TABLE `user_display` (
  `auto` int(11) NOT NULL,
  `id` bigint(20) DEFAULT NULL,
  `be` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created` timestamp NULL DEFAULT NULL,
  `text` varchar(200) DEFAULT NULL,
  `user` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_name`
--

CREATE TABLE `user_name` (
  `auto` int(11) NOT NULL,
  `user` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `text` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_password`
--

CREATE TABLE `user_password` (
  `auto` int(11) NOT NULL,
  `text` varchar(64) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `user` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `agree`
--
ALTER TABLE `agree`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `api`
--
ALTER TABLE `api`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `browser`
--
ALTER TABLE `browser`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `categories_be`
--
ALTER TABLE `categories_be`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `categories_name`
--
ALTER TABLE `categories_name`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `chat_message`
--
ALTER TABLE `chat_message`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `chat_seen`
--
ALTER TABLE `chat_seen`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `chat_to`
--
ALTER TABLE `chat_to`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `description`
--
ALTER TABLE `description`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `duration`
--
ALTER TABLE `duration`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `duration_for`
--
ALTER TABLE `duration_for`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `hash`
--
ALTER TABLE `hash`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `icons`
--
ALTER TABLE `icons`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `logs_online`
--
ALTER TABLE `logs_online`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `logs_time`
--
ALTER TABLE `logs_time`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `message_receiver`
--
ALTER TABLE `message_receiver`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `notification_room`
--
ALTER TABLE `notification_room`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `priority`
--
ALTER TABLE `priority`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `reminder`
--
ALTER TABLE `reminder`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `reminder_to`
--
ALTER TABLE `reminder_to`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `slug`
--
ALTER TABLE `slug`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `user_display`
--
ALTER TABLE `user_display`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `user_name`
--
ALTER TABLE `user_name`
  ADD PRIMARY KEY (`auto`);

--
-- Indexes for table `user_password`
--
ALTER TABLE `user_password`
  ADD PRIMARY KEY (`auto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access`
--
ALTER TABLE `access`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `agree`
--
ALTER TABLE `agree`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `api`
--
ALTER TABLE `api`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `browser`
--
ALTER TABLE `browser`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories_be`
--
ALTER TABLE `categories_be`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories_name`
--
ALTER TABLE `categories_name`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat_message`
--
ALTER TABLE `chat_message`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat_seen`
--
ALTER TABLE `chat_seen`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat_to`
--
ALTER TABLE `chat_to`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `description`
--
ALTER TABLE `description`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `duration`
--
ALTER TABLE `duration`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `duration_for`
--
ALTER TABLE `duration_for`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hash`
--
ALTER TABLE `hash`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `icons`
--
ALTER TABLE `icons`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logs_online`
--
ALTER TABLE `logs_online`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logs_time`
--
ALTER TABLE `logs_time`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `message_receiver`
--
ALTER TABLE `message_receiver`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notification_room`
--
ALTER TABLE `notification_room`
  MODIFY `auto` bigint(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `participants`
--
ALTER TABLE `participants`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `priority`
--
ALTER TABLE `priority`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reminder`
--
ALTER TABLE `reminder`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reminder_to`
--
ALTER TABLE `reminder_to`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `slug`
--
ALTER TABLE `slug`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_display`
--
ALTER TABLE `user_display`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_name`
--
ALTER TABLE `user_name`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_password`
--
ALTER TABLE `user_password`
  MODIFY `auto` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;
