<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    use HasFactory;
    protected $table = 'user_details';
    protected $primaryKey = 'auto';
    public $timestamps = false;

    protected $attributes = [
    ];

    protected $fillable = [
      'name',
      'status',
      'user',
      'fname',
      'lname',
      'dob',
    ];

    public static function table() {
		return (new self())->getTable();
	}

}
