<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;

class User extends Authenticatable {
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'user';
    protected $primaryKey = 'auto';
    public $timestamps = false;

    protected $fillable = [
        'status',
    ];

    protected $hidden = [
    ];

    protected $casts = [
    ];

    protected static function booted() {
        static::addGlobalScope('Scope', function (Builder $builder) {
            $builder->whereNotIn('status', [4]);
        });
    }

    public function password() {
        return $this->hasOne(UserPassword::class, 'user');
    }

    public function display() {
        return $this->hasOne(UserDisplay::class, 'user');
    }

    public function name() {
        return $this->hasOne(UserName::class, 'user');
    }

    public function details() {
        return $this->hasOne(UserDetails::class, 'user');
    }


}
