<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class UserPassword extends Model {
    use HasFactory;

    protected $table = 'user_password';
    protected $primaryKey = 'auto';
    public $timestamps = false;

    protected $fillable = [
        'status',
        'user',
        'text'
    ];

//     protected static function booted() {

//     }
 }
