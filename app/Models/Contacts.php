<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class Contacts extends Model {
    use HasFactory;

    protected $table = 'contacts';
    protected $primaryKey = 'auto';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'be',
        'value',
        'status',
        'type'
    ];

    protected static function booted() {
        static::addGlobalScope('scope', function (Builder $builder) {
        });
    }
}
