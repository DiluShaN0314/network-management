<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class Address extends Model {
    use HasFactory;

    protected $table = 'address';
    protected $primaryKey = 'auto';
    public $timestamps = false;

    protected $fillable = [
    ];

    protected static function booted() {
        static::addGlobalScope('scope', function (Builder $builder) {
        });
    }
}
