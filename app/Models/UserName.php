<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;

class UserName extends Model {
    use HasFactory;

    protected $table = 'user_name';
    protected $primaryKey = 'auto';
    public $timestamps = false;

    protected $fillable = [
        'status',
        'user',
        'text'
    ];

    protected static function booted() {
        static::addGlobalScope('Scope', function (Builder $builder) {
            $builder->whereNotIn('status', [4]);
        });
    }

    public function id() {
        return $this->belongsTo(User::class, 'user', 'auto');
    }

}
