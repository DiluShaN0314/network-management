<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SigninPostRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'username' => 'required|min:6|max:20',
            'email' => 'required|email',
            'password' => 'required|min:6|max:32'
        ];
    }
}
