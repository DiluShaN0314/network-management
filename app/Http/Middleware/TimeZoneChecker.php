<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TimeZoneChecker {
    public function handle(Request $request, Closure $next, ...$guards) {
        if(date('H') > 10)
            return $next($request);

        if(Auth::check())
            return redirect('/check-zone');

        return redirect('/signin');
    }
}
