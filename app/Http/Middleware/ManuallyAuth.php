<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ManuallyAuth {
    public function handle(Request $request, Closure $next, ...$guards) {
        if(Auth::check())
            return $next($request);

        return redirect('/signin');
    }
}
