<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use App\Http\Requests\SigninPostRequest;
use Auth;

class ViewFilesController extends Controller {
    public function signin() {
        return view('signin');
    }

    public function signup() {
        return view('signup');
    }

    public function homepage() {
        if(!Auth::check())
            return view('guest.home');

        return view('home');
    }

    public function dashboard() {
        return view('dashboard',[
            
        ]);
    }

    public function checkZone() {
        return view('checkTime');
    }
}
