<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\SigninPostRequest;
use App\Models\User;
use App\Models\UserPassword;
use App\Models\Contacts;
use App\Models\UserName;
use Hash;
use Auth;

class AuthController extends Controller {
    public function signup(SigninPostRequest $request) {
        $validated = $request->validated();

        $user = User::create([
            'status' => 0
        ]);

        UserPassword::create([
            'user' => $user->auto,
            'text' => Hash::make($request->password)
        ]);

        UserName::create([
            'user' => $user->auto,
            'text' => $request->username
        ]);

        Contacts::create([
            'id' => $user->auto,
            'be' => 1,
            'type' => 1,
            'value' => $request->email
        ]);
        return redirect('/signin');
    }

    public function signin(Request $request) {
        $validated = $request->validate([
            'username' => 'required|min:6|max:32',
            'password' => 'required|min:6|max:32'
        ]);

        $username = UserName::whereText($request->username)
            ->first();

        if (Hash::check($request->password, $username->id->password->text)) {
            Auth::loginUsingId($username->user);
            return redirect('/dashboard');
        } else {
            return 'miga mosamanaa password, ';
        }
    }
    public function signout(Request $request) {
        Auth::logout();
        return redirect('/signin');
    }

    public function checkauth(Request $request) {
        return Auth::check();
    }

}
