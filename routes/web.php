<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Blade;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\ViewFilesController::class, 'homepage']);

Route::group([
    'prefix' => '',
    'middleware' => [
        App\Http\Middleware\ManuallyAuth::class,
    ]
], function () {
    Route::get('/dashboard', [App\Http\Controllers\ViewFilesController::class, 'dashboard']);
});

Route::get('signup', [App\Http\Controllers\ViewFilesController::class, 'signup']);
Route::get('signin', [App\Http\Controllers\ViewFilesController::class, 'signin']);
Route::get('check-zone', [App\Http\Controllers\ViewFilesController::class, 'checkZone']);
Route::post('signup', [App\Http\Controllers\AuthController::class, 'signup']);
Route::post('signin', [App\Http\Controllers\AuthController::class, 'signin']);
Route::get('signout', [App\Http\Controllers\AuthController::class, 'signout']);
Route::get('checkauth', [App\Http\Controllers\AuthController::class, 'checkauth']);
Route::get('browserCheck', [App\Http\Controllers\AuthController::class, 'browserCheck']);

// Route::post('/profile', function () {

// })->block($lockSeconds = 10, $waitSeconds = 10);

