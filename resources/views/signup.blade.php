@extends('layout')

@section('title', 'signin title')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-6">
            <form action="/signup" class="p-5 border" method="post">
                <h5 class="text-center">Signin</h5>
                <div>
                    @csrf
                    <label for="">User name</label>
                    <input type="text" name="username" class="form-control" value="{{ old('username') }}">
                </div>
                <div>
                    <label for="">Email</label>
                    <input type="text" name="email" class="form-control" value="{{ old('email') }}">
                </div>
                <div>
                    <label for="">Password</label>
                    <input name="password" type="password" class="form-control">
                </div>
            @if ($errors->any())
                <div class="">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <div>
                    <button class="btn btn-primary w-100">Signin</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection